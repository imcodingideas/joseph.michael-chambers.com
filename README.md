# Joseph Chambers

This is my personal website that follows the [JAMstack architecture](https://jamstack.org) by using Git as a single source of truth, and [Netlify](https://www.netlify.com) for continuous deployment, and CDN distribution.

### Access Locally
```
$ yarn develop
```

To test the CMS locally, you'll need run a production build of the site:
```
$ npm run build
$ npm run serve
```