import React from 'react'
import Link from 'gatsby-link'
import { v4 } from 'uuid'

const HeaderLinks = [
  {
    name: 'About',
    link: '/#about'
  }, {
    name: 'Work',
    link: '/#work'
  }, {
    name: 'Skills',
    link: '/#skills'
  }, {
    name: 'Contact',
    link: '/#contact'
  }
]

const Header = () => (
  <header className='nav-header nav-is-fixed'>
    <nav className='nav-container'>
      <div className='nav-toggle'><i className='fa fa-bars' /></div>
      <div className='brand'>
        <Link to='/'><img src='/img/logo.png' /></Link>
        <span className='brand-tagline'>– Developer for hire</span>
      </div>
      <div className='nav-links'>
        {HeaderLinks.map(link => (
          <a href={link.link} key={v4()}>{link.name}</a>
        ))}
        <Link to='/#about' className='nav-phone-link'>(619) 928-5285</Link>
      </div>
    </nav>
  </header>
)

export default Header
