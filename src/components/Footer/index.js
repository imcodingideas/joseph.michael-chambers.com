import React from 'react'
import { v4 } from 'uuid'

const SocialLinks = [
  {
    name: 'linkedin',
    link: 'https://www.linkedin.com/in/josephmchambers/'
  }, {
    name: 'github-alt',
    link: 'https://github.com/imcodingideas/'

  }, {
    name: 'facebook',
    link: 'https://www.linkedin.com/in/josephmchambers'
  }, {
    name: 'stack-overflow',
    link: 'http://stackoverflow.com/users/1230013/joseph-chambers'
  }, {
    name: 'codepen',
    link: 'http://codepen.io/imcodingideas/'
  }, {
    name: 'angellist',
    link: 'https://angel.co/joseph-m-chambers'
  }, {
    name: 'free-code-camp',
    link: 'https://www.freecodecamp.com/imcodingideas'
  }, {
    name: 'twitter',
    link: 'https://twitter.com/imcodingideas'
  }
]

const Footer = () => (
  <footer className='footer black p-2'>
    <div className='container'>
      <div className='row m-0 s-center-align'>
        <div className='col s12 m6 l4 padding-tb-1'>
          <span className='text-muted'>
            Made with <i aria-hidden='true' className='fa fa-heart' /> by
            <a href='#about' className='pizazz-text'> Joseph Chambers</a>
          </span>
        </div>
        <div className='col s12 m6 l4 padding-tb-1'>
          <div className='footer-social-links'>
            {SocialLinks.map(link => (
              <a href={link.link} key={v4()} target='_blank'><i className={`fa fa-${link.name}`} /></a>
            ))}
          </div>
        </div>
        <div className='col s12 m12 l4 l-right-align padding-tb-1'>
          <span><a href='#top' className='pizazz-text'>To Top</a></span>
        </div>
      </div>
    </div>
  </footer>
)

export default Footer
