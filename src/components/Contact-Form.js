import React from 'react'

const ContactForm = () => (
  <form className='form' name='contact' action='/thanks' data-netlify='true'>
    <div className='form-group'>
      <div className='input-field col s12 m6'>
        <input id='first-name' type='text' name='firstName' placeholder='First Name' required='required' className='form-control white-text' />
        <label htmlFor='first-name'>First</label>
      </div>
      <div className='input-field col s12 m6'>
        <input id='last-name' type='text' name='lastName' placeholder='Last Name' className='form-control white-text' />
        <label htmlFor='last-name'>Last</label>
      </div>
    </div>
    <div className='form-group'>
      <div className='input-field col s12'>
        <input id='tel-input' type='tel' name='phone' placeholder='(619) 928-5285' className='form-control white-text' />
        <label htmlFor='tel-input'>Telephone</label>
      </div>
    </div>
    <div className='form-group'>
      <div className='input-field col s12'>
        <input id='email' type='email' name='email' aria-describedby='email-help' placeholder='Enter email' required='required' className='form-control white-text' />
        <label htmlFor='email'>Email</label><small id='email-help' className='form-text text-muted white-text right-align'>I'll never share your email with anyone else.</small>
      </div>
    </div>
    <div className='form-group'>
      <div className='input-field col s12'>
        <textarea id='message-box' rows='6' name='message' required='' className='materialize-textarea form-control white-text' />
        <label htmlFor='message-box'>How can I help you?</label>
      </div>
    </div>
    <div className='form-group'>
      <div className='col s12 p-2'>
        <button type='submit' className='btn btn-orange btn-block btn-large waves waves-effect waves-dark'>Email Me</button>
      </div>
    </div>
  </form>
)

export default ContactForm
