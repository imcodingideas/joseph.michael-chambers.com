import React from 'react'

const ReviewCard = ({ review }) => (
  <div className='col s12 m4'>
    <div className='card'>
      <div className='card-image'>
        <img src={review.image} alt='alt' />
      </div>
      <div className='card-content'>
        <span className='card-title'>{review.company}</span>
        <span className='card-tagline'>{review.name}</span>
        <p>{review.description}</p>
      </div>
    </div>
  </div>
)

export default ReviewCard
