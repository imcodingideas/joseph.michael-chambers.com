import React from 'react'

const ProjectCard = ({ project }) => (
  <div className='col s12 m6 l3'>
    <div className='card'>
      <div className='card-image'>
        <img src={project.image} alt='alt' />
      </div>
      <div className='card-content'>
        <span className='card-title'>{project.title}</span>
        <span className='card-tagline'>{project.tagline}</span>
        <p>{project.description}</p>
      </div>
      <div className='card-action'>
        <a href={project.link} target='_blank'
          className='btn btn-block white-text grey darken-4 waves waves-effect waves-light'>View project →</a>
      </div>
    </div>
  </div>
)

export default ProjectCard
