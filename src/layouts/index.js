import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import Header from '../components/Header'
import Footer from '../components/Footer'
import './../scss/style.scss'

const TemplateWrapper = ({ children }) => (
  <div>
    <Helmet
      title='Joseph Chambers || Full Stack Web Developer in San Diego'
      link={[
        { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700' },
        { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/devicons/1.8.0/css/devicons.min.css' },
        { rel: 'stylesheet', href: 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' }
      ]}
      meta={[
        { name: 'description', content: 'Full Stack Web Developer' }
      ]}
      script={[
        { type: 'text/javascript', src: 'https://code.jquery.com/jquery-3.2.1.min.js' },
        { type: 'text/javascript', src: 'https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-alpha.3/js/materialize.min.js' },
        { type: 'text/javascript', src: '/custom.js' }
      ]}
    />

    <Header />

    <main>
      {children()}
    </main>
    <Footer />
  </div>
)

TemplateWrapper.propTypes = {
  children: PropTypes.func
}

export default TemplateWrapper
