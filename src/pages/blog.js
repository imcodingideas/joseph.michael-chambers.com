import React, { Component } from 'react'
import Link from 'gatsby-link'
import { v4 } from 'uuid'

export default class IndexPage extends Component {

  render () {
    const { data } = this.props
    const { edges: posts } = data.allMarkdownRemark

    return (
      <section className='section'>
        <div className='container'>
          <div className='row'>
            <div className='col s12 m10 offset-m1'>
              <div className='section-headline left-align'>
                <h1 className='section-title'>My latest stories</h1>
              </div>
            </div>
          </div>
          <div className='row'>
            {posts
              .map(({ node: post }) => (
                <article
                  className='card-post col s12 m10 offset-m1'
                  key={v4()}
                >
                  <header className='card-post__header'>
                    <Link to={post.fields.slug}>
                      <div className='card-post__thumb' style={{ backgroundImage: `url(${post.frontmatter.image})` }} />
                    </Link>
                    <h2 className='card-post__title'>
                      <Link className='primary-text' to={post.fields.slug}>
                        {post.frontmatter.title}
                      </Link>
                    </h2>
                  </header>
                  <p className='card-post__excerpt'>
                    {post.excerpt}
                  </p>
                  <div className='card-post__footer'>
                    <small className='card-post__date'>{post.frontmatter.date}</small>
                    <Link className='btn white-text grey darken-4 waves waves-effect waves-light' to={post.fields.slug}>
                      Keep Reading →
                    </Link>
                  </div>
                </article>
              ))}
          </div>
        </div>
      </section>
    )
  }
}

export const pageQuery = graphql`
  query BlogQuery {
    allMarkdownRemark(
      sort: { order: DESC, fields: [frontmatter___date] },
      filter: { frontmatter: { templateKey: { eq: "blog-post" } }}
    ) {
      edges {
        node {
          excerpt(pruneLength: 250)
          id
          fields {
            slug
          }
          frontmatter {
            title
            templateKey
            date(formatString: "MMMM DD, YYYY")
            image
          }
        }
      }
    }
  }
`
