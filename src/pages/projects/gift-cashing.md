---
templateKey: project-post
title: GiftCashing
description: >-
    Full Stack JavaScript with Express/MongoDB
link: http://giftcashing.com
image: /img/thumbnail-giftcashing.jpg
---

![Gift Cashing](/img/thumbnail-giftcashing.jpg)

Full Stack JS app. I built this with Node, Express, SCSS, gulp.js. I really enjoyed building this.
