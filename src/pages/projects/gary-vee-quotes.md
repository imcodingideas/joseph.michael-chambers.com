---
templateKey: project-post
title: Inspirational quote website.
description: >-
    Free Code Camp Waypoint: I choose Gary Vaynerchuk because he's motivational, and keeps me focused.
link: http://google.com
image: /img/thumbnail-gary.jpg
---

![Inspirational](/img/thumbnail-gary.jpg)

Free Code Camp Waypoint: I choose Gary Vaynerchuk because he's motivational, and keeps me focused.
