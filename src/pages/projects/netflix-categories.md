---
templateKey: project-post
title: Netflix.
description: >-
    My 2am clone of netflixcodes.me
link: http://imcodingideas.github.io/netflix-hidden-categories/
image: /img/thumbnail-netflix.jpg
---

![Netflix](/img/thumbnail-netflix.jpg)

The user story is to filter the Hidden Categories from Netflix, and to pick a random category.
