---
templateKey: project-post
title: Frontier Performance
description: WordPress Front End Development
link: 'http://frontierperformance.ca/'
---
![Frontier Performance](/img/frontier.png)

Translated .PSD to a custom grid. Learned how to cascade the styles better, and got experience with LESS.
