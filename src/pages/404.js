import React from 'react'

const NotFoundPage = () => (
  <section className='section center-align page'>
    <div className='container'>
      <h1>NOT FOUND</h1>
      <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
    </div>
  </section>
)

export default NotFoundPage
