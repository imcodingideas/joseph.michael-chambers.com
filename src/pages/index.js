import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ProjectCard from '../components/Project-Card'
import ReviewCard from '../components/Review-Card'
import ContactForm from '../components/Contact-Form'
import { v4 } from 'uuid'

const Projects = [
  {
    image: '/img/thumbnail-giftcashing.jpg',
    title: 'GiftCashing',
    tagline: 'Full Stack JavaScript with Express/MongoDB',
    description: 'Full Stack JS app. I built this with Node, Express, SCSS, gulp.js. I really enjoyed building this.',
    link: 'http://giftcashing.com'
  }, {
    image: '/img/thumbnail-gary.jpg',
    title: 'Gary Vee Quotes',
    tagline: 'Inspirational quote website.',
    description: "Free Code Camp Waypoint: I choose Gary Vaynerchuk because he's motivational, and keeps me focused.",
    link: '#'
  }, {
    image: '/img/thumbnail-netflix.jpg',
    title: 'Netflix',
    tagline: 'My 2am clone of netflixcodes.me',
    description: 'The user story is to filter the Hidden Categories from Netflix, and to pick a random category.',
    link: 'http://imcodingideas.github.io/netflix-hidden-categories/'
  }, {
    image: '/img/frontier.png',
    title: 'Frontier',
    tagline: 'WordPress Front End Development',
    description: 'Translated .PSD to a custom grid. Learned how to cascade the styles better, and got experience with LESS.',
    link: 'http://frontierperformance.ca/'
  }
]

const WhatIDoFlow = [
  {
    name: 'Client Awareness',
    description: "Understanding my client's goals and needs is the first step to a successful project."
  }, {
    name: 'Performance',
    description: 'Providing value and the highest level of quality in every task, delivered through my combined strengths. This is how I uphold the satisfaction of my clients. Thriving for the best solution and giving you the performance needed to help your company thrive.'
  }, {
    name: 'Committed Through Completion',
    description: "Understanding the complete life-cycle of a project and respecting deadlines, I'm committed to you and your satisfaction."
  }, {
    name: 'Thriving for the Best',
    description: 'Consistently staying up-to-date and providing the most current and proven methods, allows me to develop your project efficiently and effectively, allowing you peace of mind, so you can concentrate on what you do best.'
  }
]

const Reviews = [
  {
    company: 'Starklane',
    name: 'Director of Operations at Stark Lane, Inc. Rudbekia Bach',
    description: 'Joseph was great to work with. He did a great job fixing my WordPress issues. He was very responsive and gave me expedient solutions',
    image: '/img/rudbekia-bach.jpg'
  }, {
    company: 'Beckertime',
    name: 'Director of Social Media at Nuber, Shaina Purser',
    description: 'I have worked alongside Joseph for many years. His analytical skills were a fantastic asset to the organization. His experience in SEO and web development is evident in the way he approaches even the most sophisticated tasks. He is great in dealing with difficult situations and a fantastic problem solver.',
    image: '/img/shaina-purser.jpg'
  }, {
    company: 'Saratoga Paint and Sip',
    name: 'Purveyor of Fun, Catherine Hover',
    description: 'Joseph was an immense help to my business in a jam. I was referred to him by a colleague. He made my project an priority. I was pleased with his attention to detail and responsiveness. I highly recommend him for any of your server administration needs.',
    image: '/img/catherine-hover.jpg'
  }
]

const MySkills = [
  {
    skill: 'Design',
    description: 'Aesthetic with a purpose. My websites are user-focused with the idea of using a beautiful design to attract the user and keep their attention. All designs look great on any screen no matter how large or small.',
    image: '/img/design-icon.png'
  }, {
    skill: 'Delivery',
    description: "Clean and sustainable. All sites are coded with performance and functionality in mind, no matter if it's a small business landing page or large web application, the best practices and tools are used.",
    image: '/img/delivery-icon.png'
  }, {
    skill: 'Deploy',
    description: 'From setting up the hosting and domain, with site maintenance and updates, the deployment process is a breeze.',
    image: '/img/deploy-icon.png'
  }
]

export default class IndexPage extends Component {

  render () {
    const { data } = this.props
    const { edges: projects } = data.allMarkdownRemark

    return (
      <div>
        <section id='about'>
          <div className='hero' style={{backgroundImage: `url(/img/bg-header.jpg)`}}>
            <div className='container'>
              <div className='hero-content'>
                <h1 className='hero-title'>Web Development examples below. Want to know more about what you’re seeing?</h1>
                <a href='#contact' className='waves-effect waves-orange btn btn-large white-text btn-orange'> Get in touch</a>
              </div>
            </div>
          </div>
        </section>

        <section id='work' className='section'>
          <div className='container'>
            <div className='row'>
              <div className='col s12'>
                <div className='section-headline center-align'>
                  <h2 className='section-title'>Some of my latest projects</h2>
                  <p className='section-desc'>A look at some recent projects I've had the pleasure to grow with, through their development.</p>
                  <hr className='styled' />
                </div>
              </div>
            </div>
            <div className='row'>
              {Projects.map(project => (
                <ProjectCard project={project} key={v4()} />
              ))}
            </div>
          </div>
        </section>

        <section id='howIwork' style={{backgroundImage: `url(/img/bg-work.jpg)`}} className='section section-dark'>
          <div className='container'>
            <div className='row'>
              <div className='col s12'>
                <div className='section-headline center-align'>
                  <h2 className='section-title'>The Foundation on Which I Stand</h2>
                  <p className='section-desc'>This is what I do</p>
                  <hr className='styled' />
                </div>
              </div>
            </div>
            <div className='row'>
              <div className='hiw-flow'>
                {WhatIDoFlow.map(step => (
                  <div className='hiw-item' key={v4()}>
                    <h3 className='hiw-item-title'>{step.name}</h3>
                    <div className='hiw-item-line' />
                    <p className='hiw-item-desc'>{step.description}</p>
                  </div>
                ))}
              </div>
            </div>

          </div>
        </section>

        <section id='specialities' className='section center-align'>
          <div className='container'>
            <div className='row'>
              <div className='devicons-list'>
                <i aria-hidden='true' className='devicons devicons devicons-html5 fa-5x' />
                <i aria-hidden='true' className='devicons devicons-sass fa-5x' />
                <i aria-hidden='true' className='devicons devicons-javascript_badge fa-5x' />
                <i aria-hidden='true' className='devicons devicons-nodejs fa-5x' />
                <i aria-hidden='true' className='devicons devicons-react fa-5x' />
                <i aria-hidden='true' className='devicons devicons-mongodb fa-5x' />
                <i aria-hidden='true' className='devicons devicons-wordpress fa-5x' /></div>
            </div>
          </div>
        </section>

        <section id='peoplesProjects' className='section section-light'>
          <div className='container'>
            <div className='row'>
              <div className='col s12'>
                <div className='section-headline center-align'>
                  <h2 className='section-title'>Projects And People</h2>
                  <p className='section-desc'>Projects where I had the opportunity to work together with my clients.</p>
                  <hr className='styled' />
                </div>
              </div>
            </div>
            <div className='row'>
              {Reviews.map(review => (
                <ReviewCard review={review} key={v4()} />
              ))}
            </div>
          </div>
        </section>

        <section id='skills' className='section'>
          <div className='container'>
            <div className='row'>
              <div className='col s12'>
                <div className='section-headline center-align'>
                  <h2 className='section-title'>Skills</h2>
                  <p className='section-desc'>Skills and knowledge I have gained with experience and study.</p>
                  <hr className='styled' />
                </div>
              </div>
            </div>
            <div className='row'>
              {MySkills.map(skill => (
                <div className='col s12 m4 center-align' key={v4()}>
                  <img src={skill.image} alt={skill.skill} />
                  <h3>{skill.skill}</h3>
                  <p className='lead'>{skill.description}</p>
                </div>
              ))}
            </div>
          </div>
        </section>

        <div id='contact' style={{backgroundImage: `url(/img/bg-footer.jpg)`}} className='section section section-dark'>
          <div className='container'>
            <div className='row'>
              <div className='col s12'>
                <div className='section-headline center-align'>
                  <h2 className='section-title'>Let's Talk!</h2>
                  <p className='section-desc'>Do you have a project? Let's talk about it!</p>
                  <hr className='styled' />
                </div>
              </div>
            </div>
            <div className='row'>
              <div className='section-flex'>
                <div className='section-flex-item l-align-self-end'>
                  <h2 className='white-text'>Message, call, email.</h2>
                  <hr className='styled styled-mini' />
                  <div className='row'>
                    <div className='col s12 m6'>
                      <small className='pizazz-text'>Mobile</small>
                      <h5><a href='tel:6199285285' className='white-text'>(619) 928-5285</a></h5>
                    </div>
                    <div className='col s12 m6'>
                      <small className='pizazz-text'>Current Timezone</small>
                      <h5 className='white-text'>Eastern</h5>
                    </div>
                  </div>
                  <div className='row'>
                    <div className='col s12'>
                      <small className='pizazz-text'>Email</small>
                      <h5>
                        <a href='mailto:joseph@michael-chambers.com' className='white-text'>joseph@michael-chambers.com</a>
                      </h5>
                    </div>
                  </div>
                </div>
                <div className='section-flex-item'>
                  <div className='row'>
                    <ContactForm />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    )
  }
}

IndexPage.propTypes = {
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      edges: PropTypes.array,
    }),
  }),
}

export const pageQuery = graphql`
  query ProjectQuery {
    allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] }) {
      edges {
        node {
          excerpt(pruneLength: 250)
          id
          fields {
            slug
          }
          frontmatter {
            title
            templateKey
            description
            path
            image
          }
        }
      }
    }
  }
`
