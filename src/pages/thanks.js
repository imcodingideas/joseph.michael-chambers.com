import React from 'react'

const ThanksPage = () => (
  <section className='section center-align page'>
    <div className='container'>
      <div className='thanks-message'>
        <h2>Thanks!</h2>
        <p>Thanks for contacting me. I will be in touch shortly. In the meantime you could follow me on
          <a href='https://twitter.com/imcodingideas' target='_blank'> Twitter!</a>
        </p>
      </div>
    </div>
  </section>
)

export default ThanksPage
