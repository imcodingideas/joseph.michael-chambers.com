import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { kebabCase } from 'lodash'
import Link from 'gatsby-link'
import Content, { HTMLContent } from '../components/Content'

export const BlogPostTemplate = ({
  content,
  contentComponent,
  description,
  tags,
  title,
  helmet,
}) => {
  const PostContent = contentComponent || Content

  return (
    <section className='section page-post'>
      { helmet || ''}
      <div className='container'>
        <div className='row'>
          <div className='col s12 m10 offset-m1 section-headline left-align'>
            <h1 className='section-title'>{title}</h1>
            <p>{description}</p>
          </div>
          <div className='col s12 m10 offset-m1'>
            <PostContent content={content} />
            {tags && tags.length ? (
              <div style={{ marginTop: `4rem` }}>
                <h4>Tags</h4>
                <ul className="taglist">
                  {tags.map(tag => (
                    <li key={tag + `tag`}>
                      <Link to={`/tags/${kebabCase(tag)}/`}>{tag}</Link>
                    </li>
                  ))}
                </ul>
              </div>
            ) : null}
          </div>
        </div>
      </div>
    </section>
  )
}

BlogPostTemplate.propTypes = {
  content: PropTypes.string.isRequired,
  contentComponent: PropTypes.func,
  description: PropTypes.string,
  title: PropTypes.string,
  helmet: PropTypes.instanceOf(Helmet),
}

const BlogPost = ({ data }) => {
  const { markdownRemark: post } = data

  return (<BlogPostTemplate
    content={post.html}
    contentComponent={HTMLContent}
    image={post.frontmatter.image}
    description={post.frontmatter.description}
    helmet={<Helmet title={`${post.frontmatter.title} | Blog`} />}
    tags={post.frontmatter.tags}
    title={post.frontmatter.title}
  />)
}

BlogPost.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.object,
  }),
}

export default BlogPost

export const pageQuery = graphql`
  query BlogPostByID($id: String!) {
    markdownRemark(id: { eq: $id }) {
      id
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        title
        description
        tags
      }
    }
  }
`
