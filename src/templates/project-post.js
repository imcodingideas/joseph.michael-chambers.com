import React from 'react'
import graphql from 'graphql'
import Helmet from 'react-helmet'
import Content, { HTMLContent } from '../components/Content'

export const ProjectPageTemplate = ({
  content, contentComponent, description, image, title, link, helmet
}) => {
  const ProjectContent = contentComponent || Content

  return (
    <section className='section page-post'>
      { helmet || ''}
      <div className='container'>
        <div className='row'>
          <div className='col s12 m10 offset-m1 section-headline left-align'>
            <h1 className='section-title'>{title}</h1>
            <p>{description}</p>
          </div>
          <div className='col s12 m10 offset-m1'>
            <ProjectContent content={content} />
          </div>
        </div>
      </div>
    </section>
  )
}

export default ({ data }) => {
  const { markdownRemark: post } = data

  return (<ProjectPageTemplate
    content={post.html}
    contentComponent={HTMLContent}
    image={post.frontmatter.image}
    description={post.frontmatter.description}
    helmet={<Helmet title={`Blog | ${post.frontmatter.title}`} />}
    title={post.frontmatter.title}
  />)
}

export const pageQuery = graphql`
  query ProjectPageById($id: String!) {
    markdownRemark(id: { eq: $id }) {
      id
      html
      frontmatter {
        path
        date(formatString: "MMMM DD, YYYY")
        title
        description
        image
        link
      }
    }
  }
`
