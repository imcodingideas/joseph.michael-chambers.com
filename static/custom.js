function r (f) { /in/.test(document.readyState) ? setTimeout('r(' + f + ')', 9) : f() }
r(function () {
  $(document).ready(function () {

    $('.nav-toggle').on('click', function () {
      $('.nav-links').toggleClass('show-links')

      if ($('.nav-toggle i').hasClass('fa-bars')) {
        $('.nav-toggle i').removeClass('fa-bars')
        $('.nav-toggle i').toggleClass('fa-close')
      } else {
        $('.nav-toggle i').addClass('fa-bars')
        $('.nav-toggle i').removeClass('fa-close')
      };
    })

    $('.nav-links a').on('click', function () {
      $('.nav-links').removeClass('show-links')
      $(this).addClass('is-current').siblings().removeClass('is-current')

      if ($('.nav-toggle i').hasClass('fa-close')) {
        $('.nav-toggle i').addClass('fa-bars')
        $('.nav-toggle i').removeClass('fa-close')
      };
    })

    $(document).on('click', 'a', function (event) {
      $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
      }, 900)
    })

    $("a[href='#top']").click(function () {
      $('html, body').animate({ scrollTop: 0 }, 2000)
      return false
    })
  })
})
